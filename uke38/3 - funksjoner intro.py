'''funksjoner'''

# Lag en funksjon som skriver ut 'Hello World!'
# Kall på funksjonen 3 ganger.

def hello_world():
    print('Hello World!')

for i in range(3):
    hello_world()
    

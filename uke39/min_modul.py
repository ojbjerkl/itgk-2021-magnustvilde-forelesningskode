'''min_modul'''
# dette er en ferdiglagd 'modul' som vi skal bruke sammen med 'lage egne moduler'

# enkel funksjon som returner en streng
def veldig_langt_funksjonsnavn(streng, antall):
    return streng + '!'*antall

# rekursiv fibonacci (helt greit å ikke forstå denne)
def recur_fibo(n):  
   if n <= 1:  
       return n  
   else:  
       return(recur_fibo(n-1) + recur_fibo(n-2))  

# denne funksjonen brukes for å kjøre koden
def run_fibo():
    antall = int(input("Hvor mange tall? "))   
    while antall <= 0:  
       print("Skriv et positivt tall.")
       antall = int(input("Hvor mange tall? "))
    else:  
       print("Fibonacci sekvens:")  
       for i in range(antall):  
           print(recur_fibo(i))

# legg merke til at vi kun definerer funksjoner her, og ikke kjører noe som helst
